def sorting_of_pages(result)->Dict:
    for q in result.keys():
        result[q].sort(key = lambda i:i[1], reverse=True)
    return result
def formatted_output(result)-> str:
    resString = ""
    for q in result.keys():
        resString = resString + str(q) + ': '
        count = 0
        for page in result[q]:
            if count == 5:
                break
            resString = resString + str(page[0]) + ' '
            count = count + 1
        resString = resString + '\n'

    return resString
