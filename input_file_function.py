def input_file(file):
    lst = []
    file_lines = open(file,"r")
    for line in file_lines:
        if line[0] != 'E':
            line = line.replace("\n","")
            lst.append(line.split(" "))
    return lst
